package ar.blito.evs2unidad12;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String[] paises = { "Argentina","Chile","Colombia",};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

















        EditText editText = findViewById(R.id.edit_text_name);
        String nombre = editText.getText().toString();

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    Toast.makeText(MainActivity.this, charSequence.toString(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });






        ImageButton imageButton = findViewById(R.id.image_button);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Click en imagebutton", Toast.LENGTH_SHORT).show();
            }
        });


        ListView listView = findViewById(R.id.list_view);
        ArrayAdapter arrayAdapterLst = new ArrayAdapter(this, android.R.layout.simple_list_item_1,android.R.id.text1,paises);
        listView.setAdapter(arrayAdapterLst);


        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,paises);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(),paises[i] , Toast.LENGTH_LONG).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        RadioGroup btnGroup =findViewById(R.id.radio_group);

        btnGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton rb= findViewById(i);
                Toast.makeText(MainActivity.this,"Seleccionaste "+rb.getText(),Toast.LENGTH_SHORT).show();
            }
        });

        CheckBox checkBox = findViewById(R.id.check_box);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    Toast.makeText(MainActivity.this,"Checkbox Seleccionda",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(MainActivity.this,"Checkbox deseleccionada",Toast.LENGTH_SHORT).show();
            }
        });


    }
}