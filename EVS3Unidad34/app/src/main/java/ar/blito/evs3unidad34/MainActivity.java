package ar.blito.evs3unidad34;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import ar.blito.evs3unidad34.activity.FramentsActivity;
import ar.blito.evs3unidad34.databinding.ActivityMainBinding;
import ar.blito.evs3unidad34.navigate.activity.MenuFBActivity;

public class MainActivity extends AppCompatActivity {

    // Nombre de clase para la etiqueta de registro
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    /**
     * la clase ViewBinding es una característica que permite un acceso más seguro y eficiente
     * a los elementos de la interfaz de usuario en un archivo de diseño XML.
     */
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Registra el inicio del método onCreate().

        Log.d(LOG_TAG, "<------->");
        Log.d(LOG_TAG, "onCreate");

        //Obtenemos la vista raíz de la jerarquía de vistas
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        //setContentView(R.layout.activity_main);

        Button btn_parametros = binding.btnParamtros;
        btn_parametros.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), PrimeroActivity.class);
                startActivity(i);
            }
        });


        Button btn_frag_old= binding.btnFOld;
        btn_frag_old.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), FramentsActivity.class);
                startActivity(i);
            }
        });

        Button btn_fragment = binding.btnFragment;
        btn_fragment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MenuFBActivity.class);
                startActivity(i);
            }
        });

        binding.floatingActionButtonMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Hola fab", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void lanzarAcercaDe(View view) {
        Intent i = new Intent(this, AcercaDeActivity.class);
        startActivity(i);
    }

    /*
        Método en una actividad que se utiliza para crear el menú de opciones que aparece
        en la barra de aplicaciones (ActionBar) de la actividad.
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /*
    * método en una actividad que se utiliza para manejar las interacciones del usuario con los elementos
    * del menú de opciones de la barra de aplicaciones (ActionBar).
    * Este método se llama cuando el usuario selecciona un elemento del menú de opciones.
    * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Toast.makeText(this,  "Item 1", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item2:
                Toast.makeText(this, "Item 2", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "onPause");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(LOG_TAG, "onRestart");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "onStop");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }

}