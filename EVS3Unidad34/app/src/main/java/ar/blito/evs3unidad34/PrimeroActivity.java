package ar.blito.evs3unidad34;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import ar.blito.evs3unidad34.modelo.Usuario;

public class PrimeroActivity extends AppCompatActivity {

    // Nombre de clase para la etiqueta de registro
    private static final String LOG_TAG = PrimeroActivity.class.getSimpleName();

    // Etiqueta única requerida para la intención extra
    public static final String EXTRA_MENSAJE = "ar.blito.extra.MESSAGE";

    // Etiqueta única para la respuesta de intención
    public static final int TEXT_REQUEST = 1;

    //Vista EditText para el mensaje
    private EditText mMensajeEditText;

    // TextView para el encabezado de respuesta
    private TextView mRespuestaHeadTextView;
    // TextView para el cuerpo de la respuesta
    private TextView mRespuestaTextView;

    /**
     * ActivityResultLauncher es una nueva forma de manejar el resultado de una actividad o de un fragmento
     * que se ha iniciado mediante un llamado a startActivityForResult o startFragmentForResult. Fue introducido
     * en la biblioteca de AndroidX Activity 1.2.0 y es una alternativa más moderna y flexible al enfoque anterior,
     * que requería la implementación del método onActivityResult.
     *
     * En lugar de sobrescribir el método onActivityResult, ActivityResultLauncher permite que se registre
     * un ActivityResultCallback para manejar los resultados de manera asíncrona. Esto significa que el resultado
     * puede ser manejado en cualquier lugar de la actividad o fragmento y no está restringido
     * a ser manejado solo en el método onActivityResult.
     **/

    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            if (result.getResultCode() == RESULT_OK) {
                String reply = result.getData().getStringExtra(SegundoActivity.EXTRA_RESPUESTA);

                // Colocamos en visible el encabezado de respuesta.
                mRespuestaHeadTextView.setVisibility(View.VISIBLE);

                // Establece la respuesta y hazla visible.
                mRespuestaTextView.setText(reply);
                mRespuestaTextView.setVisibility(View.VISIBLE);
            }
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primero);

        // Registra el inicio del método onCreate().
        Log.d(LOG_TAG, "-------");
        Log.d(LOG_TAG, "onCreate");

        // Inicialice todas las variables de vista.
        mMensajeEditText = findViewById(R.id.editText_main);
        mRespuestaHeadTextView = findViewById(R.id.text_header_respuesta);
        mRespuestaTextView = findViewById(R.id.text_mensaje_respuesta);

    }

    /**
     * Maneja el onClick para el botón "Enviar". Obtiene el valor de la principal
     * EditText, crea una intención y lanza la segunda actividad con esa intención.
     * La intención de retorno de la segunda actividad es onActivityResult().
     *
     * @param view La vista (Botón) en la que se hizo clic.
     */
    public void launchSegundoActivity(View view) {
        Log.d(LOG_TAG, "Button clicked!");

        Intent intent = new Intent(this, SegundoActivity.class);

        String mensaje = mMensajeEditText.getText().toString();
        intent.putExtra(EXTRA_MENSAJE, mensaje);

        Usuario usuario = new Usuario(1, "Pablo", "Perez");
        intent.putExtra("Usuario", usuario);

        //startActivityForResult(intent, TEXT_REQUEST);
        mStartForResult.launch(intent);
    }

    /**
     * Mantiene el estado de actividad a través de los cambios de configuración.
     *
     * @param outState Activity state data to save
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mRespuestaHeadTextView.getVisibility() == View.VISIBLE) {
            outState.putBoolean("reply_visible", true);
            outState.putString("reply_text", mRespuestaTextView.getText().toString());
        }
    }

    /**
     * Método que se llama cuando una actividad se recrea después de haber sido destruida por el
     * sistema operativo debido a una rotación de pantalla u otra configuración del dispositivo.
     * Este método se utiliza para restaurar el estado de la actividad antes de que se destruyera.
     **/
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        boolean isVisible = savedInstanceState.getBoolean("reply_visible");
        if (isVisible) {
            mRespuestaHeadTextView.setVisibility(View.VISIBLE);
            mRespuestaTextView.setText(savedInstanceState.getString("reply_text"));
            mRespuestaTextView.setVisibility(View.VISIBLE);
        }
    }
    //
    //
    //
//      Restaurar el estado.
//        if (savedInstanceState != null) {
//            boolean isVisible = savedInstanceState.getBoolean("reply_visible");
//            if (isVisible) {
//                mRespuestaHeadTextView.setVisibility(View.VISIBLE);
//                mRespuestaTextView.setText(savedInstanceState.getString("reply_text"));
//                mRespuestaTextView.setVisibility(View.VISIBLE);
//            }
//        }
//
//
//**
//     * Maneja los datos en la intención de devolución de SegundaActivity.
//     *
//     * @param requestCode Código para la solicitud SegundaActivity.
//     * @param resultCode Código que regresa de SegundaActivity.
//     * @param data Datos de intención enviados desde SegundaActivity.
//     */
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        // Pruebe la respuesta de intención correcta.
//        if (requestCode == TEXT_REQUEST) {
//            // Prueba para asegurarte de que el resultado de la respuesta de intención fue bueno.
//            if (resultCode == RESULT_OK) {
//                String reply = data.getStringExtra(SegundoActivity.EXTRA_RESPUESTA);
//
//                // Haz visible el encabezado de respuesta.
//                mRespuestaHeadTextView.setVisibility(View.VISIBLE);
//
//                // Establece la respuesta y hazla visible.
//                mRespuestaTextView.setText(reply);
//                mRespuestaTextView.setVisibility(View.VISIBLE);
//            }
//        }
//    }

}