package ar.blito.evs3unidad34;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import ar.blito.evs3unidad34.modelo.Usuario;

/**
 * SecondActivity define la segunda actividad en la aplicación. se lanza
 * de una intención con un mensaje y envía una intención de vuelta con un segundo
 * mensaje.
 */
public class SegundoActivity extends AppCompatActivity {
    //Nombre de clase para la etiqueta de registro.
    private static final String LOG_TAG
            = SegundoActivity.class.getSimpleName();

    //Etiqueta única para la respuesta de intención.
    public static final String EXTRA_RESPUESTA = "ar.blito.extra.RESPUESTA";

    //EditText para la respuesta.
    private EditText mRespuesta;

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart");
    }

    /**
     * Initializes the activity.
     *
     * @param savedInstanceState The current state data.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segundo);

        // Inicializa las variables de vista.
        mRespuesta = findViewById(R.id.editText_segundo);

        // Obtener la intención que inició esta actividad y el mensaje en
        // la intención adicional.
        Intent intent = getIntent();

        String mensaje = intent.getStringExtra(PrimeroActivity.EXTRA_MENSAJE);

        Usuario usuario  = (Usuario) intent.getSerializableExtra("Usuario");


        //colocar el mensaje en text_message TextView.
        TextView textView = findViewById(R.id.text_mensaje);
        textView.setText(usuario.getNombre()+": "+mensaje);
    }

    /**
     * Maneja el onClick para el botón "Responder". Recibe el mensaje del
     * segundo EditText, crea una intención y devuelve ese mensaje a
     * la actividad principal.
     *
     * @param view The view (Button) that was clicked.
     */
    public void returnRespuesta(View view) {
        // Obtener el mensaje de respuesta del texto de edición.
        String reply = mRespuesta.getText().toString();

        // Crear una nueva intención para la respuesta, agregarle el mensaje de respuesta
        // como extra, establezca el resultado de la intención y cierre la actividad.
        Intent replyIntent = new Intent();
        replyIntent.putExtra(EXTRA_RESPUESTA, reply);
        setResult(RESULT_OK, replyIntent);
        Log.d(LOG_TAG, "Fin SecondActivity");
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "onPause");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(LOG_TAG, "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }
}
