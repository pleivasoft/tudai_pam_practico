package ar.blito.evs3unidad34.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import ar.blito.evs3unidad34.R;
import ar.blito.evs3unidad34.fragment.FragmentPrimero;

public class FramentsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_framents);

        Bundle bundle = new Bundle();
        bundle.putString(FragmentPrimero.ARG_PARAM1, "Hola Frg OLD primero");

        /*
           getSupportFragmentManager() es un método utilizado para obtener el FragmentManager.
           es responsable de administrar los fragmentos en una actividad
        */
        FragmentManager fragmentManager = getSupportFragmentManager();

        /*
            beginTransaction() método utilizado para iniciar una transacción de fragmento.
            Las transacciones de fragmento son operaciones que te permiten agregar, reemplazar, quitar o realizar otras
            acciones relacionadas con fragmentos en una actividad
            .replace() método que se usa comúnmente dentro de una transacción de fragmento. Este método reemplaza
            un fragmento existente en una actividad con uno nuevo
        */
        fragmentManager.beginTransaction()
                .replace(R.id.frgview, FragmentPrimero.class, bundle)
                .setReorderingAllowed(true)
                .addToBackStack("name") // name can be null
                .commit();
    }
}