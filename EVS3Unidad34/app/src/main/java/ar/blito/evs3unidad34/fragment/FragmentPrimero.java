package ar.blito.evs3unidad34.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ar.blito.evs3unidad34.R;
import ar.blito.evs3unidad34.databinding.FragmentFirstBinding;


public class FragmentPrimero extends Fragment {

    public static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private FragmentFirstBinding binding;
    public FragmentPrimero() {
        // Required empty public constructor
    }

    public static FragmentPrimero newInstance(String param1, String param2) {
        FragmentPrimero fragment = new FragmentPrimero();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            Log.i("PARAM", "Se recibe parametros");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_primero, container, false);
       TextView textView = view.findViewById(R.id.textViewFrg);
       textView.setText(mParam1);
       return view;

    }
}