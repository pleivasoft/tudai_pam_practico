package ar.blito.evs3unidad34.navigate.activity;

import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import ar.blito.evs3unidad34.R;
import ar.blito.evs3unidad34.databinding.ActivityMenuFbactivityBinding;

public class MenuFBActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMenuFbactivityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMenuFbactivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);

        /*
            Navigation.findNavController() es un método utilizado utiliza para obtener
            el NavController asociado a una vista o a un fragmento.
            Componente de navegación de Android Jetpack.
            Es responsable de manejar la navegación dentro de la aplicación,
            lo que incluye la transición entre diferentes destinos (como fragmentos o actividades)
            según las acciones del usuario, como hacer clic en un botón o deslizar hacia atrás.
        */
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_menu_fbactivity);

        /*
            appBarConfiguration objeto que se utiliza para configurar la barra de aplicaciones (AppBar)
            en una actividad que utiliza el componente de navegación.
        */
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();

        /*
            NavigationUI.setupActionBarWithNavController() método se utiliza para configurar automáticamente
            la barra de aplicaciones (AppBar) para que refleje la navegación gestionada por un NavController en una actividad.
        * */
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Toast.makeText(this,  "Item 1", Toast.LENGTH_LONG).show();
                break;
            case R.id.item2:
                Toast.makeText(this, "Item 2", Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_menu_fbactivity);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }
}