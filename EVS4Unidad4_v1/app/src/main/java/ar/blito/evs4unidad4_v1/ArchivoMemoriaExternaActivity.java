package ar.blito.evs4unidad4_v1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ArchivoMemoriaExternaActivity extends AppCompatActivity {

    private EditText etArchivoSD,etDatosSD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archivo_memoria_externa);
        etArchivoSD=findViewById(R.id.etArchivoSD);
        etDatosSD=findViewById(R.id.etDatosSD);
    }
    public void grabarME(View v) {

        /**
         * getExternalFilesDir(null)
         *   Devuelve la ruta absoluta del directorio de archivos privados de tu aplicación en el
         *   almacenamiento externo del dispositivo.
         *   Este directorio es una ubicación de almacenamiento externa privada específica de la aplicación,
         *   que solo la aplicación puede acceder. Los archivos almacenados en este directorio
         *   son eliminados automáticamente cuando el usuario desinstala la aplicación
         *
        **/
        File dirtorioApp =getExternalFilesDir(null);
        grabarArchivo(dirtorioApp);

    }
    public void grabarEnDoc(View v) {
        /**
         * Environment devuelve un objeto File que representa un directorio público
         *   de almacenamiento externo en el dispositivo.
         *   Este método acepta un parámetro que indica el tipo de directorio que se desea obtener.
         *   ( DIRECTORY_DOWNLOADS,DIRECTORY_PICTURES....)
         * La ruta devuelta por getExternalStoragePublicDirectory() se refiere a una ubicación
         *   de almacenamiento externo que es pública, lo que significa que otros usuarios y
         *   aplicaciones pueden acceder a ella. Por lo tanto,
         * los archivos almacenados en este directorio
         * pueden ser vistos y editados por otros usuarios y aplicaciones.
         *
         * **/
        File carpetaDownload = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        grabarArchivo(carpetaDownload);
    }
    /**
     *OutputStreamWriter
     *  es una clase que se utiliza para escribir datos de caracteres en un flujo de salida,
     *  como un archivo en el almacenamiento interno o externo del dispositivo.
     *  Esta clase se utiliza generalmente en combinación con la clase FileOutputStream o BufferedOutputStream.
     *FileOutputStream es una clase que se utiliza para escribir datos en un archivo.
     *  Es una subclase de OutputStream, que es una clase abstracta
     *  que proporciona un conjunto de métodos para escribir bytes en un flujo de salida.
     *BufferedOutputStream es una clase que se utiliza para proporcionar un buffer o
     *  memoria temporal a un flujo de salida (OutputStream) para mejorar el rendimiento de las operaciones de escritura.
     *  En lugar de escribir directamente en el flujo de salida, se escriben primero los datos en un buffer interno y,
     *  cuando el buffer está lleno o se llama al método flush(), se escriben los datos en el flujo de salida
     * */

    public void grabarArchivo(File directorio) {
        String nomarchivo = etArchivoSD.getText().toString();
        String contenido = etDatosSD.getText().toString();
        try {
            File file =new  File(directorio, nomarchivo);

            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(file));
            osw.write(contenido);
            osw.flush();
            osw.close();
            Toast.makeText(this, "Los datos fueron grabados correctamente",
                    Toast.LENGTH_SHORT).show();
            etArchivoSD.setText("");
            etDatosSD.setText("");
        } catch (IOException ioe) {
            Toast.makeText(this, "No se pudo grabar",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * FileInputStream es una clase que se utiliza para leer datos de un archivo. Es una subclase de InputStream,
     *   que es una clase abstracta que proporciona un conjunto de métodos para leer bytes de un flujo de entrada.
     *
     *InputStreamReader es una clase que se utiliza para convertir bytes en caracteres.
     *   Se utiliza en conjunto con una instancia de InputStream para leer datos de entrada desde una fuente externa,
     *   como un archivo o una conexión de red, y convertirlos en caracteres que se pueden procesar.
     *
     * */
    public void recuperarME(View v) {
        File file =new  File(getExternalFilesDir(null), etArchivoSD.getText().toString());
        try {
            FileInputStream fIn = new FileInputStream(file);
            InputStreamReader archivo = new InputStreamReader(fIn);
            BufferedReader br = new BufferedReader(archivo);
            String linea = br.readLine();
            String todo = "";
            while (linea != null) {
                todo = todo + linea + " ";
                linea = br.readLine();
            }
            br.close();
            archivo.close();
            etDatosSD.setText(todo);

        } catch (IOException e) {
            Toast.makeText(this, "No se pudo leer",
                    Toast.LENGTH_SHORT).show();
        }
    }
}