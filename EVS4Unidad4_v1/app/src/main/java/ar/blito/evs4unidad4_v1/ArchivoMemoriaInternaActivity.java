package ar.blito.evs4unidad4_v1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ArchivoMemoriaInternaActivity extends AppCompatActivity {

    private EditText etDatos;
    private EditText edNombreArchivo;
    private String nombreArchivo="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archivo_memoria_interna);
        etDatos=findViewById(R.id.etmDatos);
        edNombreArchivo=findViewById(R.id.editNombreArchivo);
    }
/**
 *
 * openFileOutput() es un método de la clase Context en Android que se utiliza para crear un archivo
 *    en el almacenamiento interno privado de la aplicación y obtener un objeto FileOutputStream
 *    que se puede utilizar para escribir datos en el archivo.
 *
 * InputStreamReader es una clase que se utiliza para convertir bytes en caracteres.
 *    Se utiliza en conjunto con una instancia de InputStream para leer datos de entrada desde una fuente externa,
 *    como un archivo o una conexión de red, y convertirlos en caracteres que se pueden procesar.
 *
 * BufferedReader es una clase que se utiliza para leer texto desde una fuente de entrada de caracteres,
 *    como un archivo o una conexión de red. Esta clase se utiliza para leer texto en búfer,
 *    lo que permite una lectura más eficiente de los datos de entrada.
 *
 * OutputStreamWriter
 *    es una clase que se utiliza para escribir datos de caracteres en un flujo de salida,
 *    como un archivo en el almacenamiento interno o externo del dispositivo.
 *    Esta clase se utiliza generalmente en combinación con la clase FileOutputStream o BufferedOutputStream.
 *
 * */
    public void grabarMI(View v) {
        nombreArchivo = edNombreArchivo.getText().toString();
        try {
            OutputStreamWriter archivo = new OutputStreamWriter(openFileOutput(nombreArchivo, Activity.MODE_PRIVATE));
            archivo.write(etDatos.getText().toString());
            archivo.flush();
            archivo.close();
            edNombreArchivo.setText("");
            etDatos.setText("");
        } catch (IOException e) {
        }
        Toast t = Toast.makeText(this, "Los datos fueron grabados",Toast.LENGTH_SHORT);
        t.show();
        //finish();
    }
    public void leerFile(View v) {
        nombreArchivo = edNombreArchivo.getText().toString();
        etDatos.setText("");
        if (nombreArchivo!="" && existe( nombreArchivo))
            try {
                InputStreamReader archivo = new InputStreamReader(openFileInput(nombreArchivo));
                BufferedReader br = new BufferedReader(archivo);
                String linea = br.readLine();
                String todo = "";
                while (linea != null) {
                    todo = todo + linea + "\n";
                    linea = br.readLine();
                }
                br.close();
                archivo.close();
                etDatos.setText(todo);
            } catch (IOException e) {
            }
    }
    private boolean existe(String archbusca) {
        String[] archivos = fileList();
        for (int f = 0; f < archivos.length; f++)
            if (archbusca.equals(archivos[f]))
                return true;
        return false;
    }
}