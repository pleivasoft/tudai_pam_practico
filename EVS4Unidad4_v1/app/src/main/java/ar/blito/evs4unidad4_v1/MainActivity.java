package ar.blito.evs4unidad4_v1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import ar.blito.evs4unidad4_v1.Usuario.UsuarioActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void arrancarSPActivity(View v) {
        Intent intent = new Intent(this, SharedPreferencesActivity.class);
        startActivity(intent);
    }

    public void arrancarAMIActivity(View v) {
        Intent intent = new Intent(this, ArchivoMemoriaInternaActivity.class);
        startActivity(intent);
    }

    public void arrancarASDActivity(View v) {
        Intent intent = new Intent(this, ArchivoMemoriaExternaActivity.class);
        startActivity(intent);
    }
    public void arrancarSqliteActivity(View v) {
        Intent intent = new Intent(this, UsuarioActivity.class);
        startActivity(intent);
    }


}