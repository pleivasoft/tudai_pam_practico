package ar.blito.evs4unidad4_v1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

public class SharedPreferencesActivity extends AppCompatActivity {

    private TextView etKey, etDato;
    private EditText etVerTodos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);
        etKey=findViewById(R.id.etKey);
        etDato=findViewById(R.id.etDato);
        etVerTodos=findViewById(R.id.etVerTodos);
    }

    public void grabarMap(View v) {

        String key=etKey.getText().toString();
        String dato=etDato.getText().toString();
        SharedPreferences preferencias=getSharedPreferences("agenda", Context.MODE_PRIVATE);
        preferencias.edit().putString(key,dato).apply();
        etKey.setText("");
        etDato.setText("");
        Toast.makeText(this,"Datos grabados", Toast.LENGTH_LONG).show();
    }

    public void obtenerDato(View v) {
        String key=etKey.getText().toString();
        SharedPreferences prefe=getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String valor=prefe.getString(key, "");
        if (valor.length()==0) {
            Toast.makeText(this,"No existe dicho nombre en la agenda", Toast.LENGTH_LONG).show();
        }
        else {
            etDato.setText(valor);
        }
    }

    public void verTodos(View v){
        SharedPreferences preferences =getSharedPreferences("agenda", Context.MODE_PRIVATE);
        Map<String, ?> allEntries = preferences.getAll();
        String datos ="";
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            datos+="Key:"+entry.getKey() + "\nDatos: " + entry.getValue().toString().replace("\n","\n\t\t\t\t\t\t\t")+"\n";
            datos+="*******************************************\n";
            Log.d("Valores", entry.getKey() + ": " + entry.getValue().toString());
        }
        etVerTodos.setText(datos);
    }

    public void limpiarTodo(View v){
        SharedPreferences preferences =getSharedPreferences("agenda", Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
        etVerTodos.setText("");
        etKey.setText("");
        etDato.setText("");
    }
}