package ar.blito.evs4unidad4_v1.Usuario;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import ar.blito.evs4unidad4_v1.R;
import ar.blito.evs4unidad4_v1.sqlite.adaptadores.ListaUsuariosAdapter;
import ar.blito.evs4unidad4_v1.sqlite.db.DbUsuarios;
import ar.blito.evs4unidad4_v1.sqlite.entidades.Usuario;

public class UsuarioActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    SearchView txtBuscar;
    RecyclerView listaUsuarios;
    ArrayList<Usuario> listaArrayUsuarios;
    FloatingActionButton fabNuevo;
    ListaUsuariosAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);
        txtBuscar = findViewById(R.id.txtBuscar);
        listaUsuarios = findViewById(R.id.listaUsuarios);
        fabNuevo = findViewById(R.id.favNuevo);

        listaUsuarios.setLayoutManager(new LinearLayoutManager(this));

        cargarLista();


        fabNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nuevoRegistro();
            }
        });

        txtBuscar.setOnQueryTextListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        cargarLista();
        Log.i("resume","volviiii");
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    public  void cargarLista(){

        DbUsuarios dbUsuarios = new DbUsuarios(UsuarioActivity.this);

        listaArrayUsuarios = new ArrayList<>();

        adapter = new ListaUsuariosAdapter(dbUsuarios.mostrarUsuarios());
        listaUsuarios.setAdapter(adapter);
    }
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menuNuevo:
                nuevoRegistro();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void nuevoRegistro(){
        Intent intent = new Intent(this, NuevoActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        adapter.filtrado(s);
        return false;
    }
}