package ar.blito.evs4unidad4_v1.sharedpreferences;

public interface IMySharedPreferences {
    String getString(String key, String defaultValue);
    void putString(String key, String value);
}
