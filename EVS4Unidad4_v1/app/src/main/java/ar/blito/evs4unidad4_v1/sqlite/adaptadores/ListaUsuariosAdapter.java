package ar.blito.evs4unidad4_v1.sqlite.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ar.blito.evs4unidad4_v1.R;
import ar.blito.evs4unidad4_v1.Usuario.VerActivity;
import ar.blito.evs4unidad4_v1.sqlite.entidades.Usuario;


/**
 * ADAPTADOR
 *  GENERAR Y ASISNAR CADA UO DE LOS ELEMENTOS QUE VA TENER LA LISTA
 * RecyclerView.Adapter es una clase abstracta en Android que se utiliza como base para crear adaptadores personalizados para RecyclerView.
 * El adaptador personalizado es responsable de proporcionar los datos para la vista de lista y
 * también de crear las vistas que se mostraránen la lista.
 *
 * Para utilizar RecyclerView.Adapter, debes crear una clase que extienda esta clase abstracta y
 * anular los métodos necesarios para crear las vistas de lista y proporcionar los datos. Estos son los métodos principales que debes anular:
 *
 * onCreateViewHolder: Este método se llama cuando se necesita crear una nueva vista para la lista. Debes crear y devolver una nueva instancia de la clase RecyclerView.ViewHolder que represente la vista de lista. La vista de lista se puede inflar desde un archivo XML o crear programáticamente.
 *
 * onBindViewHolder: Este método se llama cuando se necesita mostrar los datos en una vista de lista. Debes actualizar la vista de lista con los datos proporcionados en el objeto de datos correspondiente.
 *
 * getItemCount: Este método devuelve el número total de elementos en la lista.
 *
 * */
public class ListaUsuariosAdapter extends RecyclerView.Adapter<ListaUsuariosAdapter.UsuarioViewHolder> {

    ArrayList<Usuario> listaUsuarios;
    ArrayList<Usuario> listaOriginal;

    public ListaUsuariosAdapter(ArrayList<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
        listaOriginal = new ArrayList<>();
        listaOriginal.addAll(listaUsuarios);
    }

    //NOS AYUDA A DEFINIR CUAL VA SER EL DISEÑO DE CADA ELEMENTO DE LA LISTA
    @NonNull
    @Override
    public UsuarioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_item_usuario, null, false);
        return new UsuarioViewHolder(view);
    }

    //ASIGNAR LOS ELEMENTOS QUE CORRESPONDEN PARA CADA OPCION
    @Override
    public void onBindViewHolder(@NonNull UsuarioViewHolder holder, int position) {
        holder.viewNombre.setText(listaUsuarios.get(position).getNombre());
        holder.viewTelefono.setText(listaUsuarios.get(position).getTelefono());
        holder.viewCorreo.setText(listaUsuarios.get(position).getCorreo_electornico());
    }

    @Override
    public int getItemCount() {
        return listaUsuarios.size();
    }

    public void filtrado(final String txtBuscar) {
        int longitud = txtBuscar.length();
        if (longitud == 0) {
            listaUsuarios.clear();
            listaUsuarios.addAll(listaOriginal);
        } else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                List<Usuario> collecion = listaUsuarios.stream()
                        .filter(i -> i.getNombre().toLowerCase().contains(txtBuscar.toLowerCase()))
                        .collect(Collectors.toList());
                listaUsuarios.clear();
                listaUsuarios.addAll(collecion);
            } else {
                for (Usuario c : listaOriginal) {
                    if (c.getNombre().toLowerCase().contains(txtBuscar.toLowerCase())) {
                        listaUsuarios.add(c);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

 /**
 *  clase interna estática que extiende RecyclerView.ViewHolder. En el constructor,
 *  se obtiene una referencia a los TextView en la vista de lista y en el método bind, se actualiza los TextView con el nombre correspondiente.
 * */
    public class UsuarioViewHolder extends RecyclerView.ViewHolder {

        TextView viewNombre, viewTelefono, viewCorreo;

        public UsuarioViewHolder(@NonNull View itemView) {
            super(itemView);

            viewNombre = itemView.findViewById(R.id.viewNombre);
            viewTelefono = itemView.findViewById(R.id.viewTelefono);
            viewCorreo = itemView.findViewById(R.id.viewCorreo);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, VerActivity.class);
                    intent.putExtra("ID", listaUsuarios.get(getAdapterPosition()).getId());
                    context.startActivity(intent);
                }
            });
        }
    }
}
