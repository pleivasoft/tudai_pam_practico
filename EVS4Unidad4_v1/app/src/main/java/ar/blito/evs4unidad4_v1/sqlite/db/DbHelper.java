package ar.blito.evs4unidad4_v1.sqlite.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;
/**
 * SQLiteOpenHelper
 *  es una clase de utilidad que se utiliza para crear y actualizar bases de datos SQLite.
 *  Esta clase proporciona métodos que permiten crear una base de datos SQLite, actualizar su esquema,
 *  y proporcionar acceso a la base de datos para su uso por otras partes de una aplicación
 * */
public class DbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NOMBRE = "evs4_unidad_4.db";
    public static final String TABLE_USUARIOS = "t_usuarios";

    public DbHelper(@Nullable Context context) {
        super(context, DATABASE_NOMBRE, null, DATABASE_VERSION);
    }

    // crear tablas y otros objetos de la base de datos
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_USUARIOS + "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nombre TEXT NOT NULL," +
                "telefono TEXT NOT NULL," +
                "password TEXT NOT NULL," +
                "correo_electronico TEXT)");
    }

    // actualizar esquema de la base de datos
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE " + TABLE_USUARIOS);
        onCreate(sqLiteDatabase);

    }
}

