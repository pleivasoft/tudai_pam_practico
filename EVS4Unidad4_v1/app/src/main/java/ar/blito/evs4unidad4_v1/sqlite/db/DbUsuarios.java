package ar.blito.evs4unidad4_v1.sqlite.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import ar.blito.evs4unidad4_v1.sqlite.entidades.Usuario;

public class DbUsuarios extends DbHelper {

    Context context;

    public DbUsuarios(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    /**
     * getWritableDatabase()
     *      es un método que se utiliza para obtener una instancia de SQLiteDatabase
     *      que permite realizar operaciones de escritura en una base de datos SQLite (insertar, actualizar, eliminar y consultar).
     *      Esta instancia se obtiene a través de una subclase de SQLiteOpenHelper, que se encarga de crear y actualizar la base de datos.
     *      Si la base de datos aún no se ha creado, este método llamará automáticamente al método onCreate() de la subclase SQLiteOpenHelper.
     *      Si la base de datos ya existe, este método abrirá la base de datos y llamará al método onUpgrade() si la versión de la base de datos ha cambiado.
     *
     * ContentValues()
     *      clase se utiliza para almacenar un conjunto de valores que se pueden insertar o actualizar
     *      en una base de datos SQLite a través de un objeto SQLiteDatabase.
     *      Esta clase representa una fila de datos de una tabla en la base de datos y
     *      proporciona un mecanismo para agregar pares de clave-valor a esa fila.
     *
     * */
    public long insertarContacto(String nombre, String telefono, String password, String correo_electronico) {
        long id = 0;
        try {
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("nombre", nombre);
            values.put("telefono", telefono);
            values.put("password", password);
            values.put("correo_electronico", correo_electronico);

            id = db.insert(TABLE_USUARIOS, null, values);
            db.close();
        } catch (Exception ex) {
            ex.toString();
        }

        return id;
    }

    /**
     * rawQuery()
     *  es un método de la clase SQLiteDatabase que se utiliza para ejecutar consultas SQL personalizadas en una base de datos SQLite.
     *  Este método se llama "raw" porque se espera que la consulta SQL esté en formato de cadena sin procesar y sin validación.
     *
     * Con parametros
     *      String sql = "SELECT nombre FROM usuarios WHERE correo_electornico > ?";
     *      String[] selectionArgs = { "lalalalala@jajaja.com" };
     *      Cursor cursor = db.rawQuery(sql, selectionArgs);
     *
     *  Cursor
     *      es una interfaz que se utiliza para representar un conjunto de resultados de una consulta a una base de datos SQLite.
     *      Un objeto Cursor proporciona un mecanismo para iterar sobre los resultados de la consulta y
     *      acceder a los valores de las columnas en cada fila.
     * */

    public ArrayList<Usuario> mostrarUsuarios() {

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ArrayList<Usuario> listaUsuario = new ArrayList<>();
        Usuario usuario;
        Cursor cursorUsuarios;

        cursorUsuarios = db.rawQuery("SELECT * FROM " + TABLE_USUARIOS + " ORDER BY nombre ASC", null);

        if (cursorUsuarios.moveToFirst()) {
            do {
                usuario = new Usuario();
                usuario.setId(cursorUsuarios.getInt(0));
                usuario.setNombre(cursorUsuarios.getString(1));
                usuario.setTelefono(cursorUsuarios.getString(2));
                usuario.setPassword(cursorUsuarios.getString(3));
                usuario.setCorreo_electornico(cursorUsuarios.getString(4));
                listaUsuario.add(usuario);
            } while (cursorUsuarios.moveToNext());
        }

        cursorUsuarios.close();
        db.close();
        return listaUsuario;
    }

    /**
    // Creamos una instancia de SQLiteDatabase
    SQLiteDatabase db = dbHelper.getReadableDatabase();

    // Definimos los campos que queremos obtener de la tabla "usuarios"
    String[] campos = { "nombre", "correo_electronico", "telefono" };

    // Definimos la cláusula WHERE que especifica el valor de la clave primaria
    String where = "id = ?";

    // Definimos los valores que se van a reemplazar en la cláusula WHERE
    String[] valoresWhere = { "1" };

    // Ejecutamos la consulta y obtenemos un cursor para recorrer los resultados
    Cursor cursor = db.query(TABLE_USUARIOS, campos, where, valoresWhere, null, null, null);

    // Verificamos si encontramos un resultado
    if (cursor.moveToFirst()) {
        // Obtenemos los valores de los campos de la fila encontrada
        // Hacemos algo con los valores obtenidos, como mostrarlos en una vista
    } else {
        // No se encontró ninguna fila con la clave primaria especificada
    }

    // Cerramos el cursor y la conexión a la base de datos
    cursor.close();
    db.close();
    *
    * */


    public Usuario verUsuario(int id) {

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Usuario usuario = null;
        Cursor cursorUsuario;

        cursorUsuario = db.rawQuery("SELECT * FROM " + TABLE_USUARIOS + " WHERE id = " + id + " LIMIT 1", null);

        if (cursorUsuario.moveToFirst()) {
            usuario = new Usuario();
            usuario.setId(cursorUsuario.getInt(0));
            usuario.setNombre(cursorUsuario.getString(1));
            usuario.setTelefono(cursorUsuario.getString(2));
            usuario.setPassword(cursorUsuario.getString(3));
            usuario.setCorreo_electornico(cursorUsuario.getString(4));
        }
        cursorUsuario.close();
        db.close();
        return usuario;
    }
/**
*
*    // Creamos una instancia de SQLiteDatabase
*    SQLiteDatabase db = dbHelper.getWritableDatabase();
*
*    // Valores actualizados que queremos establecer
*    ContentValues values = new ContentValues();
*    values.put("correo_electronico", "juan.nuevo_email@example.com");
*
*    // Actualizamos la fila correspondiente en la tabla "usuarios"
*    int filasActualizadas = db.update(TABLE_USUARIOS, values, "nombre = ? AND telefono = ?", new String[] { "Juan", "123456" });
*
*    // Verificamos si la actualización fue exitosa
*    if (filasActualizadas > 0) {
*        // La fila fue actualizada exitosamente
*    } else {
*        // No se pudo encontrar la fila que se iba a actualizar
*    }
*
 * */
    public boolean editarUsuario(int id, String nombre, String telefono, String correo_electronico) {

        boolean correcto = false;

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            db.execSQL("UPDATE " + TABLE_USUARIOS + " SET nombre = '" + nombre + "', telefono = '" + telefono + "', correo_electronico = '" + correo_electronico + "' WHERE id='" + id + "' ");
            correcto = true;
        } catch (Exception ex) {
            ex.toString();
            correcto = false;
        } finally {
            db.close();
        }

        return correcto;
    }

    /**
     *
     // Creamos una instancia de SQLiteDatabase
     SQLiteDatabase db = dbHelper.getWritableDatabase();

     // Definimos la cláusula WHERE que especifica el valor de la clave primaria
     String where = "id = ?";

     // Definimos los valores que se van a reemplazar en la cláusula WHERE
     String[] valoresWhere = {"1"};

     // Ejecutamos la consulta para eliminar la fila y obtenemos el número de filas afectadas
     int filasAfectadas = db.delete("usuarios", where, valoresWhere);

     // Verificamos si se eliminó alguna fila
     if(filasAfectadas >0)
     {
        // La fila se eliminó correctamente
     } else{
        // No se eliminó ninguna fila con la clave primaria especificada
     }

     // Cerramos la conexión a la base de datos
     db.close();
     *
     * */

    public boolean eliminarUsuario(int id) {

        boolean correcto = false;
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + TABLE_USUARIOS + " WHERE id = '" + id + "'");
            correcto = true;
        } catch (Exception ex) {
            ex.toString();
            correcto = false;
        } finally {
            db.close();
        }

        return correcto;
    }
}
