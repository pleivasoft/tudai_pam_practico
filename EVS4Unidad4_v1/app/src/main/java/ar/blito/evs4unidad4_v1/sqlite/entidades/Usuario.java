package ar.blito.evs4unidad4_v1.sqlite.entidades;

public class Usuario {

    private int id;
    private String nombre;
    private String telefono;
    private String password;
    private String correo_electornico;

    public Usuario() {
    }

    public Usuario(int id, String nombre, String telefono, String correo_electornico) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.correo_electornico = correo_electornico;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCorreo_electornico() {
        return correo_electornico;
    }

    public void setCorreo_electornico(String correo_electornico) {
        this.correo_electornico = correo_electornico;
    }
}
