package ar.blito.evs5unindad5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import android.Manifest;

public class GrabarActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {
    private static final int REQUEST_PERMISSION = 1;
    private String filePath = "";
    private MediaRecorder mediaRecorder;
    TextView tv1;
    MediaRecorder recorder;
    MediaPlayer player;
    File archivo;
    Button b1, b2, b3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grabar);

        // Verificar permisos de grabación de audio
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            // Solicitar permisos de grabación de audio si no están otorgados
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_PERMISSION);
        } else {
            // Iniciar grabación de audio

        }
        tv1 = findViewById(R.id.textView);
        b1 = findViewById(R.id.button);
        b2 = findViewById(R.id.button2);
        b3 = findViewById(R.id.button3);
    }


    public void grabarMR(View v) {
        recorder = new MediaRecorder();
        // Establecer la fuente de datos de la grabación (por ejemplo, el micrófono)
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);

        // Establecer el formato de salida del archivo (por ejemplo, MPEG_4)
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

        // Establecer el archivo de salida
        filePath = getExternalFilesDir(null).getAbsolutePath() + "/audio_record.3gp";

        // Establecer la ruta de destino para el archivo grabado
        recorder.setOutputFile(filePath);

        // Configurar el codificador de audio
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            // Preparar y comenzar la grabación
            recorder.prepare();
            // Iniciar la grabación
            recorder.start();
            Toast.makeText(this, "Grabando audio...", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        tv1.setText("Grabando.....");
        b1.setEnabled(false);
        b2.setEnabled(true);
    }

    public void detenerMR(View v) {
        // Detener la grabación y liberar los recursos
        recorder.stop();
        recorder.release();
        player = new MediaPlayer();
        player.setOnCompletionListener(this);
        try {
            player.setDataSource(filePath);
        } catch (IOException e) {
        }
        try {
            player.prepare();
        } catch (IOException e) {
        }
        b1.setEnabled(true);
        b2.setEnabled(false);
        b3.setEnabled(true);
        tv1.setText("Listo para reproducir");
    }

    public void reproducirP(View v) {
        player.start();
        b1.setEnabled(false);
        b2.setEnabled(false);
        b3.setEnabled(false);
        tv1.setText("Reproduciendo");
    }

    public void onCompletion(MediaPlayer mp) {
        b1.setEnabled(true);
        b2.setEnabled(true);
        b3.setEnabled(true);
        tv1.setText("Listo");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                // Permiso de grabación de audio denegado
                Toast.makeText(this, "Permiso de grabación de audio denegado", Toast.LENGTH_SHORT).show();
            }
        }
    }
}