package ar.blito.evs5unindad5;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

public class GrabarNativoActivity extends AppCompatActivity {

    Uri url1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grabar_nativo);
    }
    /**
     *  Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
     * crea un nuevo objeto Intent que se utiliza para iniciar la grabación de audio utilizando una
     * aplicación externa en dispositivos Android. Al utilizar esta intención, puedes permitir que
     * los usuarios graben audio utilizando la aplicación de grabación de audio predeterminada de su dispositivo.
     * */
    public void grabar(View v) {
        Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        if (intent.resolveActivity(getPackageManager()) != null) {
            mStartForResult.launch(intent);
        } else {
            Toast.makeText(GrabarNativoActivity.this, "No se encontró una aplicación para grabar sonido", Toast.LENGTH_SHORT).show();
        }

    }

    public void reproducir(View v) {
        MediaPlayer mediaPlayer = MediaPlayer.create(this, url1);
        mediaPlayer.start();
    }

    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            if (result.getResultCode() == RESULT_OK) {
                url1 = result.getData().getData();
            }
        }
    });

}