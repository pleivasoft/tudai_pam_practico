package ar.blito.evs5unindad5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    MediaPlayer mp;
    Button b5;
    int posicion = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b5 = findViewById(R.id.button5);

    }

    public void destruir() {
        if (mp != null) mp.release();
    }

    /**
     * primero llama al método destruir (para el caso que el mp3 este en ejecución actualmente) seguidamente
     * creamos un objeto de la clase MediaPlayer llamando al método create (en este hacemos referencia al archivo que
     * copiamos a la carpeta raw) Llamamos al método start. Por último extraemos el texto del quinto botón y verificamos
     * si la reproducción debe ejecutarse en forma circular
     */
    public void iniciar(View v) {
        destruir();
        mp = MediaPlayer.create(this, R.raw.numeros);
        mp.start();
        String op = b5.getText().toString();
        if (op.equals("no reproducir en forma circular")) mp.setLooping(false);
        else mp.setLooping(true);
    }

    /**
     * verifica que el objeto de la clase MediaPlayer este creado y en ejecución, en caso afirmativo
     * recuperamos la posición actual de reproducción y llamamos seguidamente al método pause
     */
    public void pausar(View v) {
        if (mp != null && mp.isPlaying()) {
            posicion = mp.getCurrentPosition();
            mp.pause();
        }
    }

    /**
     * verifica que el objeto de la clase MediaPlayer este creado y la propiedad isPlaying retorne false
     * para proceder a posicionar en que milisegundo continuar la reproducción
     */
    public void continuar(View v) {
        if (mp != null && mp.isPlaying() == false) {
            mp.seekTo(posicion);
            mp.start();
        }
    }

    /**
     * detener interrumpe la ejecución del mp3 e inicializa el atributo posicion con cero
     */
    public void detener(View v) {
        if (mp != null) {
            mp.stop();
            posicion = 0;
        }
    }

    public void circular(View v) {
        detener(null);
        String op = b5.getText().toString();
        if (op.equalsIgnoreCase("no reproducir en forma circular"))
            b5.setText("reproducir en forma circular");
        else b5.setText("no reproducir en forma circular");
    }

    public void ejecutar(View v) {
        detener(null);
        String path = getExternalFilesDir(null).toString();
        Uri datos = Uri.parse(path + "/airflow.mp3");
        mp = MediaPlayer.create(this, datos);
        mp.start();
    }

    public void ejecutarInternet(View v) {
        detener(null);
        initializeMediaPlayer();
        try {
            //// Establecer la fuente de datos del archivo multimedia que se reproducirá
            mp.setDataSource("https://cdn.pixabay.com/audio/2022/03/09/audio_4f7f74d60f.mp3");
            mp.prepareAsync();
            mp.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void initializeMediaPlayer() {
        mp = new MediaPlayer();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mp.setAudioAttributes(new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setLegacyStreamType(AudioManager.STREAM_MUSIC)
                    .build());
        } else {
            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
        try {
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}