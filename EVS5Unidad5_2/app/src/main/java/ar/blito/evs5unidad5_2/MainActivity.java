package ar.blito.evs5unidad5_2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener{

    private int corx, cory;
    private Lienzo fondo;
    /**
     * obtenemos la referencia del ConstraintLayout que tiene el Activity. Creamos un objeto de la clase L
     * ienzo (llamado fondo) y agregamos el objeto fondo al ConstraintLayout llamando al método addView
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        corx = 100;
        cory = 100;
        ConstraintLayout layout1 = findViewById(R.id.layout1);
        fondo = new Lienzo(this);
        fondo.setOnTouchListener(this);
        layout1.addView(fondo);
    }

    /**
     * Debemos crear una clase que herede de la clase View (todos los controles visuales en Android
     * heredan de esta clase) y sobreescribir el método onDraw.
     * El método onDraw se ejecuta cuando tiene que graficarse. Para acceder a las primitivas gráficas
     * hay una clase llamada Canvas que encapsula todo lo relacionado a pintar píxeles, líneas, rectángules etc.:
     */
    class Lienzo extends View {

        public Lienzo(Context context) {
            super(context);
        }

        /**
         * onDraw(Canvas canvas) es un método que se encuentra en las clases que heredan de View,
         * como TextView, ImageView o cualquier vista personalizada que crees.
         * Este método se utiliza para dibujar los elementos visuales de la vista en el lienzo (Canvas),
         * que es el área en la que se representará la vista en la pantalla.
         *
         * Lo primero que hacemos en el método onDraw es obtener el ancho y alto en píxeles del dispositivo
         * mediante los métodos getWidth() y getHeight() que nos provee la clase Canvas. Seguidamente creamos
         * un objeto de la clase Paint. Llamamos al método setARGB para definir el color del pincel
         * (el primer parámetro indica el valor de transparencia con 255 indicamos que no hay transparencia,
         * luego indicamos la cantidad de rojo, verde y azul.
         * <p>
         * Por último debemos llamar al método drawPoint que dibuja el píxel en la columna, fila y pincel que
         * le pasamos como parámetros.
         */
        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            int ancho = canvas.getWidth();
            int alto = canvas.getHeight();

            //Color de fondo
            canvas.drawRGB(255, 183, 71);

            Paint pincel1 = new Paint();

//            /**  Pixel en centro de la pantalla */
//            pincel1.setStrokeWidth(50);
//            pincel1.setColor(Color.BLUE);
//            canvas.drawPoint(ancho / 2, alto / 2, pincel1);
//
//            /** Pintamos Lineas en la pantalla */
//            //Linea Roja Vertical
//            pincel1.setStrokeWidth(4);
//            pincel1.setColor(Color.RED);
//            canvas.drawLine(70, 0, 70, alto, pincel1);
//
//            //Linea Negra Horizontal
//            pincel1.setColor(Color.BLACK);
//            pincel1.setStrokeWidth(4);
//
//             canvas.drawLine(0, 60, ancho, 60, pincel1);
//            int cantLineas = alto / 30 - 2;
//            for (int fila = 0; fila < cantLineas; fila++) {
//                canvas.drawLine(0, fila * 30 + 60, ancho, fila * 30 + 60,
//                        pincel1);
//            }

//            /**
//             * Creamos un rectangulo
//             * Dibujamos un rectángulo desde la coordenada columna: 10 y fila 10 hasta la columna que coincide
//             * con el ancho de la pantalla menos 10 píxeles y la fila 40
//             * */
//            pincel1.setColor(Color.GREEN);
//            canvas.drawRect(10, 10, ancho - 10, 40, pincel1);
//
//            /** Pinte el perímetro llamando al método setStyle*/
//            pincel1.setStyle(Paint.Style.STROKE);
//            /** Configuramos el grosor del lapiz llamando al método setStrokeWidth */
//            pincel1.setStrokeWidth(10);
//            canvas.drawRect(100, 1210, ancho - 100, 140, pincel1);

            /**
             * Creamos un circulos
             */
//            pincel1.setColor(Color.MAGENTA);
//            pincel1.setStyle(Paint.Style.STROKE);
//            pincel1.setStrokeWidth(5);
//            /**
//            * Definimos un for para dibujar los 10 círculos concéntricos
//            * (indicamos en los dos primeros parámetros el punto central del círculo y en el tercer parámetro el radio del círculo:
//            */
//            for (int f = 0; f < 10; f++) {
//                canvas.drawCircle(ancho / 2, alto / 2, f * 40, pincel1);
//            }


            /**
             * Creamos un texto

             Para graficar texto disponemos del método drawText que nos permite imprimir un String
             en una determinada columna, fila con un determinado pincel que podemos definir su color
             */

//            pincel1.setARGB(255, 255, 0, 0);
//            //tamaño de la letra:
//            pincel1.setTextSize(50);
//            //estilo de la letra:
//            pincel1.setTypeface(Typeface.SERIF);
//            canvas.drawText("Hola Mundo (SERIF)", 0, 70, pincel1);
//
//            pincel1.setTypeface(Typeface.SANS_SERIF);
//            canvas.drawText("Hola Mundo (SANS SERIF)", 0, 120, pincel1);
//
//            pincel1.setTypeface(Typeface.MONOSPACE);
//            canvas.drawText("Hola Mundo (MONOSPACE)", 0, 170, pincel1);
//
//            Typeface tf = Typeface.create(Typeface.SERIF, Typeface.ITALIC);
//            pincel1.setTypeface(tf);
//            canvas.drawText("Hola Mundo (SERIF ITALIC)", 0, 220, pincel1);
//
//            tf = Typeface.create(Typeface.SERIF, Typeface.BOLD_ITALIC);
//            pincel1.setTypeface(tf);
//            canvas.drawText("Hola Mundo (SERIF ITALIC BOLD)", 0, 280, pincel1);

            /**
             *  texto sobre un camino
             * */
            //Creamos un objeto de la clase Path e indicamos el primer punto del camino llamando al método moveTo:
//            Path camino = new Path();
//            camino.moveTo(0, alto / 2);
//            // indicamos todos los otros puntos en forma consecutiva llamando al método lineTo
//            camino.lineTo(40, alto / 2 - 30);
//            camino.lineTo(80, alto / 2 - 60);
//            camino.lineTo(120, alto / 2 - 90);
//            camino.lineTo(160, alto / 2 - 120);
//            camino.lineTo(220, alto / 2 - 150);
//            camino.lineTo(280, alto / 2 - 180);
//            camino.lineTo(340, alto / 2 - 210);
//            camino.lineTo(400, alto / 2 - 240);
//            camino.lineTo(500, alto / 2 - 270);
//            camino.lineTo(340, alto / 2 - 300);
//            pincel1.setARGB(255, 255, 0, 0);
//            pincel1.setTextSize(50);
//            canvas.drawTextOnPath("Hola Mundo Hola Mundo", camino, 0, 0,
//                    pincel1);

            /**
             * Evento touch: dibujar un círculo
             * */
//            pincel1.setARGB(255, 255, 0, 0);
//            pincel1.setStrokeWidth(4);
//            pincel1.setStyle(Paint.Style.STROKE);
//            canvas.drawCircle(corx, cory, 50, pincel1);

        }

    }


    public boolean onTouch(View v, MotionEvent event) {
        corx = (int) event.getX();
        cory = (int) event.getY();
        fondo.invalidate();
        return true;
    }
}