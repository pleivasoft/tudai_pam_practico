package ar.blito.evs6unidad5_3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class GPSActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_LOCATION_PERMISSION = 1;

    private Button btnActivarGPS;
    private Button btnVerMApa;
    private TextView coordinatesTextView;

    //Es una clase en Android que se utiliza para interactuar con los servicios de ubicación del dispositivo.
    //Proporciona métodos para acceder a información de ubicación y recibir actualizaciones de ubicación del dispositivo, 
    //ya sea a través de GPS, redes celulares o proveedores de ubicación específicos.
    private LocationManager locationManager;
    // interfaz LocationListener se utiliza para escuchar eventos relacionados con la ubicación, como cambios de ubicación, 
    //actualizaciones periódicas de ubicación o cambios en el estado del proveedor de ubicación
    private LocationListener locationListener;

    private double latitude;
    private double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);

        coordinatesTextView = findViewById(R.id.coordinatesTextView);
        btnActivarGPS = findViewById(R.id.btnActivarGPS);
        btnVerMApa = findViewById(R.id.btnVerMapa);
        btnActivarGPS.setOnClickListener(this);
        btnVerMApa.setOnClickListener(this);
        
        /**
         * El LocationManager es una clase del framework de Android que se utiliza para acceder a servicios y
         * obtener información relacionada con la ubicación en un dispositivo Android. 
         * Al obtener una instancia de LocationManager, puedes utilizar sus métodos para solicitar actualizaciones de ubicación,
         * registrar receptores de ubicación y acceder a proveedores de ubicación disponibles en el dispositivo.
        */
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        /**
         * LocationListener es una interfaz en Android que se utiliza para recibir actualizaciones de ubicación del LocationManager. 
         * Permite a una aplicación escuchar los cambios de ubicación en tiempo real y responder a ellos.
         */
        locationListener = new LocationListener() {
            //Se llama cuando se detecta un cambio en la ubicación. Recibe como argumento un objeto Location que contiene la nueva ubicación.
            @Override
            public void onLocationChanged(Location location) {
                showCoordinates(location.getLatitude(), location.getLongitude());
            }
            //Se llama cuando el estado del proveedor de ubicación cambia.
            // Proporciona información sobre el proveedor de ubicación y su nuevo estado.
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
            //Se llama cuando un proveedor de ubicación se habilita (por ejemplo, se enciende el GPS).
            @Override
            public void onProviderEnabled(String provider) {
            }
            //Se llama cuando un proveedor de ubicación se deshabilita (por ejemplo, se apaga el GPS).
            @Override
            public void onProviderDisabled(String provider) {
            }
        };
    }

    private void checkPermissionGPS() {
        //se utiliza para realizar una validación en tiempo de ejecución y verificar si la versión del sistema operativo Android en el dispositivo es igual o superior a la versión Marshmallow (Android 6.0),
        // que tiene asignado el código de versión Build.VERSION_CODES.M.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //método utilizado en Android para comprobar si una aplicación tiene un permiso específico otorgado por el usuario. 
            //Este método es parte de la clase ActivityCompat, que proporciona utilidades para trabajar con permisos en actividades.
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                startLocationListener();
            } else {
                //método utilizado en Android para solicitar permisos al usuario en tiempo de ejecución.
                // Este método se encuentra en la clase ActivityCompat y se utiliza en conjunción con el método onRequestPermissionsResult() para manejar la respuesta del usuario a la solicitud de permisos.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION_PERMISSION);
            }
        } else {
            //Pedimo los permisos en tiempo de ejecucion o suponemmos que se otorgaron en la instalacion
            startLocationListener();
        }
    }

    private void startLocationListener() {
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    0, 0, locationListener);
            }else{
                Toast.makeText(this, "No dispone  de permisos al GPS", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void stopLocationListener() {
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
    }

    private void showCoordinates(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        String coordinates = "Latitude: " + latitude + "\nLongitude: " + longitude;
        coordinatesTextView.setText(coordinates);
    }
    /**
        Método de devolución de llamada que se utiliza en una actividad de Android para manejar la respuesta del usuario a una solicitud de 
        permisos realizada mediante ActivityCompat.requestPermissions(). Este método se debe sobrescribir en la actividad para recibir la 
        respuesta del sistema sobre la solicitud de permisos.
    */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationListener();
            } else {
                Toast.makeText(this, "Location permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationListener();
    }

    @Override
    public void onClick(View view) {
        int id= view.getId();
        if(id==R.id.btnVerMapa){
            openMapActivity(view);
        }else if(id==R.id.btnActivarGPS){
            checkPermissionGPS();
        }
    }

    public void openMapActivity(View view) {
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("extra_latitude", latitude);
        intent.putExtra("extra_longitude", longitude);
        startActivity(intent);
    }
}