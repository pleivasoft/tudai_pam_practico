package ar.blito.evs6unidad5_3;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.Manifest;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
/**
 *  interfaz en Android que se utiliza para manejar eventos de clic en elementos de la interfaz de usuario (UI).
 *  Al implementar esta interfaz y sobrescribir su método onClick(View v),
 *  puedes definir el comportamiento que deseas que ocurra cuando se hace clic en un elemento.
 * */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_PERMISSION_CAMERA = 100;
    private static final int REQUEST_PERMISSION_WRITE_STORAGE = 200;

    Bitmap bitmapDecoded;
    private Button btnCapturaFoto;
    private Button btnGrabarFoto;
    private ImageView imageView;

    private Uri photoUri;

    /**
     * ActivityResultLauncher
     * Es una interfaz que nos permite lanzar una actividad y recibir el resultado
     * de manera más conveniente.
     */
    private ActivityResultLauncher<Uri> mStartForResultLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intitUI();
    }

    /**
     * registerForActivityResult
     * método introducido en la biblioteca de Android Jetpack Activity Result,
     * que proporciona una forma simplificada y basada en el resultado de
     * interactuar con actividades externas y
     * recibir resultados de ellas en componentes de la aplicación, como actividades
     * o fragmentos.
     */
    private void intitUI() {
        imageView = findViewById(R.id.imageView);
        btnCapturaFoto = findViewById(R.id.capturarFoto);
        btnGrabarFoto = findViewById(R.id.grabarFoto);
        btnCapturaFoto.setOnClickListener(this);
        btnGrabarFoto.setOnClickListener(this);
        mStartForResultLauncher = registerForActivityResult(new ActivityResultContracts.TakePicture(), result -> {
            if (result) {
                try {
                    // obtenemos un objeto Bitmap a partir de una URI que representa una imagen
                    // almacenada en la galería.
                    // getContentResolver() es un método que devuelve el ContentResolver asociado a
                    // la aplicación actual.
                    // El ContentResolver es una clase que proporciona una interfaz para interactuar
                    // con el proveedor de contenido de la aplicación
                    // y acceder a los datos almacenados en el sistema.
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri);
                    setImageview(getResizedBitmap(bitmap, 1024));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            } else {
                // El resultado no fue exitoso o se canceló
                // Maneja la falla o cancelación aquí
            }
        });
    }

    /**
     * Método de devolución de llamada utilizado en Android para manejar eventos de
     * clic en elementos interactivos, como botones, vistas de lista, elementos de
     * menú, etc.
     * Este método se debe implementar cuando se define un objeto OnClickListener o
     * se establece un OnClickListener en un elemento interactivo.
     */
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.capturarFoto) {
            checkPermissionCamera();
        } else if (id == R.id.grabarFoto) {
            checkPermissionstorage();
        }
    }

    private void checkPermissionCamera() {
        // se utiliza para realizar una validación en tiempo de ejecución y verificar si
        // la versión del sistema operativo Android en el dispositivo es igual o
        // superior a la versión Marshmallow (Android 6.0),
        // que tiene asignado el código de versión Build.VERSION_CODES.P/M.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // método utilizado en Android para comprobar si una aplicación tiene un permiso
            // específico otorgado por el usuario.
            // Este método es parte de la clase ActivityCompat, que proporciona utilidades
            // para trabajar con permisos en actividades.
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                tomarFoto();
            } else {
                // método utilizado en Android para solicitar permisos al usuario en tiempo de
                // ejecución.
                // Este método se encuentra en la clase ActivityCompat y se utiliza en
                // conjunción con el método
                // onRequestPermissionsResult() para manejar la respuesta del usuario a la
                // solicitud de permisos.
                ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA },
                        REQUEST_PERMISSION_CAMERA);
            }
        } else {
            // Pedimo los permisos en tiempo de ejecucion o suponemmos que se otorgaron en
            // la instalacion
            tomarFoto();
        }
    }

    /**
     * La función launch() de ActivityResultLauncher se utiliza para iniciar la
     * actividad con el resultado esperado.
     * En este caso, launch(photoUri) inicia la actividad de captura de imágenes,
     * utilizando la URI de la imagen como parámetro.
     */
    private void tomarFoto() {
        photoUri = createImageUri();
        mStartForResultLauncher.launch(photoUri);
    }

    /**
     * ContentValues
     * establecemos los valores necesarios para el título, nombre de visualización y
     * tipo MIME de la imagen, etc.
     *
     * getContentResolver().insert
     *  se utiliza para insertar un nuevo registro en un proveedor de contenido en Android.
     *  Un proveedor de contenido es una interfaz para compartir datos entre aplicaciones.
     *  proporciona acceso a los contenidos de la galería de imágenes y a los archivos de imágenes almacenados en el dispositivo.
     *
     * MediaStore.Images.Media
     *  Al utilizar la clase MediaStore.Images.Media, puedes realizar diversas operaciones relacionadas con las imágenes,
     *  como consultar las imágenes almacenadas, obtener información sobre ellas, acceder a sus metadatos, crear miniaturas,
     *  entre otras.
     *
     */
    private Uri createImageUri() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + ".jpg";
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, imageFileName);
        values.put(MediaStore.Images.Media.DISPLAY_NAME, imageFileName);
        values.put(MediaStore.Images.Media.RELATIVE_PATH, Environment.DIRECTORY_DCIM + "/Camera");
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        return getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    private void setImageview(Bitmap resizedBitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        bitmapDecoded = BitmapFactory.decodeStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        imageView.setImageBitmap(bitmapDecoded);
    }
    
    /**
        Método de devolución de llamada que se utiliza en una actividad de Android para manejar la respuesta del usuario a una solicitud de 
        permisos realizada mediante ActivityCompat.requestPermissions(). Este método se debe sobrescribir en la actividad para recibir la 
        respuesta del sistema sobre la solicitud de permisos.
    */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA) {
            if (permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                tomarFoto();
            } else {
                // Toast
            }
        } else if (requestCode == REQUEST_PERMISSION_WRITE_STORAGE) {
            if (permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                grabarFoto();
            } else {
                // Toast
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void checkPermissionstorage() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    grabarFoto();
                } else {
                    ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                            REQUEST_PERMISSION_WRITE_STORAGE);
                }
            } else {
                // Pedimo los permisos en tiempo de ejecucion o suponemmos que se otorgaron en
                // la instalacion
                grabarFoto();
            }
        } else {
            // Pedimo los permisos en tiempo de ejecucion o suponemmos que se otorgaron en
            // la instalacion
            grabarFoto();
        }
    }

    private void grabarFoto() {
        OutputStream outputStream = null;
        File file = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentResolver resolver = getContentResolver();
            ContentValues values = new ContentValues();

            String fileNombre = System.currentTimeMillis() + "image_example";

            values.put(MediaStore.Images.Media.DISPLAY_NAME, fileNombre);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            values.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/MyApp");
            values.put(MediaStore.Images.Media.IS_PENDING, 1);
            Uri collection = MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY);
            Uri imageUri = resolver.insert(collection, values);
            try {
                outputStream = resolver.openOutputStream(imageUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            values.clear();
            values.put(MediaStore.Images.Media.IS_PENDING, 0);
            resolver.update(imageUri, values, null, null);

        } else {
            String imageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            String fileNombre = System.currentTimeMillis() + ".jpg";
            file = new File(imageDir, fileNombre);
            try {
                outputStream = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        boolean isGrabado = bitmapDecoded.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        if (isGrabado) {
            Toast.makeText(this, "Foto grabada", Toast.LENGTH_SHORT).show();
        }
        if (outputStream != null) {
            try {
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        if (file != null)
            MediaScannerConnection.scanFile(this, new String[] { file.toString() }, null, null);
    }

    private Bitmap getResizedBitmap(Bitmap bitmap, int maxSize) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= maxSize && height <= maxSize)
            return bitmap;

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height / bitmapRatio);
        }
        return Bitmap.createScaledBitmap(bitmap, width, height, true);

    }

}
