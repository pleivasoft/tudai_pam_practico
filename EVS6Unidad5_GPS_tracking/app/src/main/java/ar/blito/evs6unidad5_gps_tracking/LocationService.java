package ar.blito.evs6unidad5_gps_tracking;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class LocationService extends Service  {

    public static final String ACTION_LOCATION_UPDATE = "ACTION_LOCATION_UPDATE";
    public static final String EXTRA_LATITUDE = "EXTRA_LATITUDE";
    public static final String EXTRA_LONGITUDE = "EXTRA_LONGITUDE";
    private LocationManager locationManager;
    private LocationListener locationListener;
    private double previousLatitude;
    private double previousLongitude;


    @Override
    public void onCreate() {
        super.onCreate();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                // Verificar si la distancia es mayor a 5 metros desde la última coordenada
                float[] results = new float[1];
                Location.distanceBetween(previousLatitude, previousLongitude, latitude, longitude, results);
                float distance = results[0];

                //if (distance > 5) {
                    //llamar endpoint
                    Log.d("LocationService", "New coordinates: " + latitude + ", " + longitude);
                    handleLocationUpdate(location);
                    previousLatitude = latitude;
                    previousLongitude = longitude;
                //}
            }

        };
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Iniciar la escucha de actualizaciones de ubicación
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Si no tienes el permiso, muestra un mensaje o solicita el permiso al usuario
            Log.e("LocationService", "No tienes el permiso necesario para acceder al GPS.");
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Detener la escucha de actualizaciones de ubicación
        locationManager.removeUpdates(locationListener);
    }


    private final IBinder binder = new LocalBinder();

    public class LocalBinder extends Binder {
        LocationService getService() {
            return LocationService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
    private LocationUpdateListener locationUpdateListener;
    // Método para establecer el LocationUpdateListener
    public void setLocationUpdateListener(LocationUpdateListener listener) {
        locationUpdateListener = listener;
    }

    // Método para obtener las actualizaciones de ubicación y notificar al listener
    private void handleLocationUpdate(Location location) {
        if (locationUpdateListener != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            locationUpdateListener.onLocationUpdate(latitude, longitude);
        }
    }

}
