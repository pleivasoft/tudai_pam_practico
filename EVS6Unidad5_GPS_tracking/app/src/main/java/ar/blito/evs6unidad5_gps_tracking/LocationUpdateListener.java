package ar.blito.evs6unidad5_gps_tracking;

public interface LocationUpdateListener {
    void onLocationUpdate(double latitude, double longitude);
}
