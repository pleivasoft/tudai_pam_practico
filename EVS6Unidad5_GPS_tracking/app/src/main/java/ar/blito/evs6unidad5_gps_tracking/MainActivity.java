package ar.blito.evs6unidad5_gps_tracking;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements LocationUpdateListener {

    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private TextView textView;
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.editTextTextMultiLine);
        checkPermissionGPS();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isBound) {
            unbindService(serviceConnection);
            isBound = false;
        }
    }

    private void checkPermissionGPS() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                startService();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION_PERMISSION);
            }
        } else {
            //Pedimo los permisos en tiempo de ejecucion o suponemmos que se otorgaron en la instalacion
            startService();
        }
    }

    private void startService() {


        // Iniciar el servicio
        Intent serviceIntent = new Intent(this, LocationService.class);
        startService(serviceIntent);

        // Vincular el servicio
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
    }


    /**
     * Comunicacion por medio de un servicio vinculado
     * */

    private LocationService locationService;
    private boolean isBound = false;

    // ServiceConnection para establecer la conexión con el servicio
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            LocationService.LocalBinder binder = (LocationService.LocalBinder) iBinder;
            locationService = binder.getService();
            locationService.setLocationUpdateListener(MainActivity.this);
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isBound = false;
        }
    };
    @Override
    public void onLocationUpdate(double latitude, double longitude) {
        Log.d("MainActivity", "coordinadas: " + latitude + ", " + longitude);
        String coordinatesText = "Lat: " + latitude + ", Lng: " + longitude+"\n";
        textView.setText(textView.getText()+coordinatesText);
    }
}