package ar.blito.evs6_mvp.Interface;

import java.util.List;

import ar.blito.evs6_mvp.db.DatabaseHelper;
import ar.blito.evs6_mvp.model.Usuario;

// crear una interfaz que va a designar los roles y las tareas específicas que va a tener cada uno de las entidades
//va a tener otras tres interfaces dentro una que represente a la vista otra al presente y otra al modelo
public interface UsuariosContract {
    interface View {
        void showUsuarios(List<Usuario> usuarios);
        void showErrorMessage(String message);
        void showSuccessMessage(String message);
    }

    interface Presenter {
        void getUsuarios();
        void createUsuario(String nombre);
    }

}
