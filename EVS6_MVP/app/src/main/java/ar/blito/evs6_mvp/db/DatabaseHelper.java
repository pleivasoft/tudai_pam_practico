package ar.blito.evs6_mvp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import ar.blito.evs6_mvp.model.Usuario;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "evs6mvp";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_USUARIOS = "usuarios";
    private static final String COLUMN_USUARIO_ID = "id";
    private static final String COLUMN_USUARIO_NOMBRE = "nombre";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Crear la tabla "usuarios"
        String createUsuariosTableQuery = "CREATE TABLE " + TABLE_USUARIOS + "("
                + COLUMN_USUARIO_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_USUARIO_NOMBRE + " TEXT)";
        db.execSQL(createUsuariosTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Actualizar la base de datos si es necesario
        // Aquí puedes agregar el código para migraciones de datos
    }

    public void insertUsuario(Usuario usuario) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USUARIO_NOMBRE, usuario.getNombre());

        db.insert(TABLE_USUARIOS, null, values);

        db.close();
    }
    public List<Usuario> getAllUsuarios() {
        List<Usuario> usuarios = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USUARIOS, null);

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String nombre = cursor.getString(1);
                Usuario usuario = new Usuario(id, nombre);
                usuarios.add(usuario);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return usuarios;
    }


}
