package ar.blito.evs6_mvp.presenter;

import java.util.List;

import ar.blito.evs6_mvp.Interface.UsuariosContract;
import ar.blito.evs6_mvp.db.DatabaseHelper;
import ar.blito.evs6_mvp.model.Usuario;


public class UsuariosPresenter implements UsuariosContract.Presenter {
    //el objeto model y view dentro de ella
    //inversion de dependencnias no dependemos de la implementacion sino de la abtraccion
    private UsuariosContract.View view;
    private DatabaseHelper databaseHelper;

    //para inyecctar las dependencias por medio del constrcuctor
    public UsuariosPresenter(UsuariosContract.View view, DatabaseHelper databaseHelper) {
        this.view = view;
        this.databaseHelper = databaseHelper;
    }

    @Override
    public void getUsuarios() {
        List<Usuario> usuarios = databaseHelper.getAllUsuarios();
        view.showUsuarios(usuarios);
    }

    @Override
    public void createUsuario(String nombre) {
        // Crear un nuevo usuario
        Usuario usuario = new Usuario(1, nombre);
        databaseHelper.insertUsuario(usuario);
        view.showSuccessMessage("Usuario creado: " + nombre);
    }


}