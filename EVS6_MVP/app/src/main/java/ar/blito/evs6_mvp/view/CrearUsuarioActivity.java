package ar.blito.evs6_mvp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import ar.blito.evs6_mvp.Interface.UsuariosContract;
import ar.blito.evs6_mvp.R;
import ar.blito.evs6_mvp.db.DatabaseHelper;
import ar.blito.evs6_mvp.model.Usuario;
import ar.blito.evs6_mvp.presenter.UsuariosPresenter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CrearUsuarioActivity extends AppCompatActivity implements UsuariosContract.View {

    @BindView(R.id.edit_text_nombre)
    EditText editTextNombre;
    @BindView(R.id.button_crear_usuario)
   Button buttonCrearUsuario;
    @OnClick(R.id.button_crear_usuario)
    public void submit(View view) {
        String nombre = editTextNombre.getText().toString();
        presenter.createUsuario(nombre);
        Intent resultIntent = new Intent();
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private UsuariosContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_usuario);
        ButterKnife.bind(this);
        //editTextNombre = findViewById(R.id.edit_text_nombre);
        //buttonCrearUsuario = findViewById(R.id.button_crear_usuario);

        presenter = new UsuariosPresenter(this, new DatabaseHelper(this));

        /*buttonCrearUsuario.setOnClickListener(v -> {
            String nombre = editTextNombre.getText().toString();
            presenter.createUsuario(nombre);
            Intent resultIntent = new Intent();
            setResult(RESULT_OK, resultIntent);
            finish();
        });*/


    }
    @Override
    public void showUsuarios(List<Usuario> usuarios) {

    }
    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void showSuccessMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}