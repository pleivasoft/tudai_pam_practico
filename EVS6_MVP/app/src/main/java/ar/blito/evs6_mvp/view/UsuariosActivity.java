package ar.blito.evs6_mvp.view;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.List;

import ar.blito.evs6_mvp.Interface.UsuariosContract;
import ar.blito.evs6_mvp.R;
import ar.blito.evs6_mvp.adapter.UsuariosAdapter;
import ar.blito.evs6_mvp.db.DatabaseHelper;
import ar.blito.evs6_mvp.model.Usuario;
import ar.blito.evs6_mvp.presenter.UsuariosPresenter;

public class UsuariosActivity extends AppCompatActivity implements UsuariosContract.View {

    private UsuariosContract.Presenter presenter;
    private FloatingActionButton fabCrearUsuario;
    private RecyclerView recyclerView;
    private UsuariosAdapter usuariosAdapter;

    private ActivityResultLauncher<Intent> crearUsuarioLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuarios);
        recyclerView = findViewById(R.id.recycler_view_usuarios);

        fabCrearUsuario = findViewById(R.id.fabCrearUsuario);
        fabCrearUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UsuariosActivity.this, CrearUsuarioActivity.class);
                crearUsuarioLauncher.launch(intent);
            }
        });

        usuariosAdapter = new UsuariosAdapter();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(usuariosAdapter);

        presenter = new UsuariosPresenter(this, new DatabaseHelper(this));
        presenter.getUsuarios();

        crearUsuarioLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == RESULT_OK) {
                            // Actualizar la lista de usuarios aquí
                            presenter.getUsuarios();
                        }
                    }
                });
    }

    @Override
    public void showUsuarios(List<Usuario> usuarios) {
        usuariosAdapter.setUsuarios(usuarios);
        usuariosAdapter.notifyDataSetChanged();
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSuccessMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}