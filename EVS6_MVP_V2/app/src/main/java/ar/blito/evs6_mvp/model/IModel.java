package ar.blito.evs6_mvp.model;
import java.util.List;

public interface IModel {
    List<Usuario> getUsuarios();
    void insertUsuario(Usuario usuario);
}
