package ar.blito.evs6_mvp.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import ar.blito.evs6_mvp.view.IView;
import ar.blito.evs6_mvp.db.DatabaseHelper;

public class Model implements IModel{

    private SQLiteDatabase database;

    public Model(IView iView) {
        database = new DatabaseHelper((Context) iView).getWritableDatabase();
    }

    @Override
    public List<Usuario> getUsuarios() {
        List<Usuario> usuarios = new ArrayList<>();
        Cursor cursor = this.database.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_USUARIOS, null);
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String nombre = cursor.getString(1);
                Usuario usuario = new Usuario(id, nombre);
                usuarios.add(usuario);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return usuarios;
    }

    @Override
    public void insertUsuario(Usuario usuario) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_USUARIO_NOMBRE, usuario.getNombre());
        this.database.insert(DatabaseHelper.TABLE_USUARIOS, null, values);
    }
}
