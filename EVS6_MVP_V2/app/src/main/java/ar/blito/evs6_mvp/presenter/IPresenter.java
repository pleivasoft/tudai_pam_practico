package ar.blito.evs6_mvp.presenter;

public interface IPresenter {
    void getUsuarios();
    void createUsuario(String nombre);
}
