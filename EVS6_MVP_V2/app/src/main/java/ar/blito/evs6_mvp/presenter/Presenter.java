package ar.blito.evs6_mvp.presenter;

import java.util.List;

import ar.blito.evs6_mvp.model.IModel;
import ar.blito.evs6_mvp.model.Model;
import ar.blito.evs6_mvp.model.Usuario;
import ar.blito.evs6_mvp.view.IView;

public class Presenter implements IPresenter {

    private IView iView;
    private IModel iModel;

    public Presenter(IView iView) {
        this.iView = iView;
        iModel = new Model(iView);
    }

    @Override
    public void getUsuarios() {
        List<Usuario> list = iModel.getUsuarios();
        iView.showUsuarios(list);
    }
    @Override
    public void createUsuario(String nombre) {
        // Crear un nuevo usuario
        Usuario usuario = new Usuario(1, nombre);
        iModel.insertUsuario(usuario);
        iView.showSuccessMessage("Usuario creado: " + nombre);
    }

}