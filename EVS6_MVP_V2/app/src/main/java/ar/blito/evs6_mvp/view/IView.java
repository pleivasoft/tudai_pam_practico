package ar.blito.evs6_mvp.view;

import java.util.List;

import ar.blito.evs6_mvp.model.Usuario;

public interface IView {
    void showUsuarios(List<Usuario> usuarios);
    void showSuccessMessage(String message);
}
