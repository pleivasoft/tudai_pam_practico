package com.leiva339.tudai_pam;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    String[] paises = { "Argentina", "Chile", "China", "Uruguay", "Otros"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton imageButton = findViewById(R.id.image_button);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Realizar acciones en función del clic en el botón
                Toast.makeText(MainActivity.this, "Haz clic en el botón de la imagen", Toast.LENGTH_SHORT).show();
            }
        });

















        ListView listView = findViewById(R.id.list_view);
        //Crear una lista de elementos
        List<String> elements = new ArrayList<>();
        elements.add("Elemento 1");
        elements.add("Elemento 2");
        elements.add("Elemento 3");

        //Crear un adaptador utilizando la lista de elementos
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, paises);

        // Aplicar el adaptador al ListView
        listView.setAdapter(adapter);

        //Opcionalmente, se puede utilizar el método setOnItemClickListener para escuchar los clics en los elementos del ListView.
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Obtener el elemento en la posición seleccionada
                String selectedItem = parent.getItemAtPosition(position).toString();

                // Realizar acciones en función del elemento seleccionado
                Toast.makeText(MainActivity.this, "Elemento seleccionado: " + selectedItem, Toast.LENGTH_SHORT).show();
            }
        });

















        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter adapter1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,paises);

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(),paises[i] , Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        RadioGroup btnGroup = findViewById(R.id.radio_group);

        btnGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = findViewById(i);
                Toast.makeText(MainActivity.this,"Seleccionaste "+radioButton.getText(),Toast.LENGTH_SHORT).show();
            }
        });

        CheckBox checkBox = findViewById(R.id.check_box);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                Toast.makeText(MainActivity.this,"Checkbox Seleccionda",Toast.LENGTH_SHORT).show();
                else
                Toast.makeText(MainActivity.this,"Checkbox deseleccionada ",Toast.LENGTH_SHORT).show();
            }
        });


    }

}
   /*

    implements AdapterView.OnItemSelectedListener
    spinner.setOnItemSelectedListener(this);
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(getApplicationContext(),paises[i] , Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    */
